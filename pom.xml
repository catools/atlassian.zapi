<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.catools</groupId>
    <artifactId>atlassian.zapi</artifactId>
    <version>0.1.39</version>
    <packaging>jar</packaging>

    <scm>
        <tag>HEAD</tag>
        <url>scm:git:https://gitlab.com/catools/atlassian.zapi.git</url>
    </scm>

    <name>Core Automation Toolset - Atlassian Zephyr Client</name>
    <description>The Atlassian Zephyr client</description>
    <url>https://gitlab.com/catools/atlassian.zapi.git</url>

    <developers>
        <developer>
            <name>Alireza Keshmiri</name>
            <email>kimiak2000@gmail.com</email>
            <url>https://www.linkedin.com/in/akeshmiri</url>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>http://www.opensource.org/licenses/mit-license.php</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>

        <!-- Versions -->
        <jmockit.version>1.44</jmockit.version>
        <catools.version>0.1.39</catools.version>

        <!-- Core -->
        <CONFIGS_TO_LOAD>defaultConfig.yaml,zApiDefaultConfig.yaml</CONFIGS_TO_LOAD>

        <!-- ZApi -->
        <ZAPI_HOME></ZAPI_HOME>
        <ZAPI_USERNAME></ZAPI_USERNAME>
        <ZAPI_PASSWORD></ZAPI_PASSWORD>
        <ZAPI_DATE_FORMAT></ZAPI_DATE_FORMAT>
        <ZAPI_DATE_SPLITTER></ZAPI_DATE_SPLITTER>
        <ZAPI_CYCLE_NAME></ZAPI_CYCLE_NAME>
        <ZAPI_STATUS_MAP></ZAPI_STATUS_MAP>
        <ZAPI_SEARCH_BUFFER_SIZE></ZAPI_SEARCH_BUFFER_SIZE>

        <!-- Repository Path -->
        <mvn.repo.url>https://repo.maven.apache.org/maven2</mvn.repo.url>
        <mvn.spring.repo.url>https://repo.spring.io/plugins-release/</mvn.spring.repo.url>
        <repo.snapshot.url>https://oss.sonatype.org/content/repositories/snapshots</repo.snapshot.url>
        <repo.release.url>https://oss.sonatype.org/service/local/staging/deploy/maven2</repo.release.url>
    </properties>
    <repositories>
        <repository>
            <id>maven</id>
            <url>${mvn.repo.url}</url>
        </repository>
        <repository>
            <id>snapshot</id>
            <url>${repo.snapshot.url}</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
            </snapshots>
        </repository>
        <repository>
            <id>spring</id>
            <url>${mvn.spring.repo.url}</url>
        </repository>
    </repositories>
    <distributionManagement>
        <snapshotRepository>
            <id>snapshot</id>
            <url>${repo.snapshot.url}</url>
        </snapshotRepository>
        <repository>
            <id>central</id>
            <url>${repo.release.url}</url>
        </repository>
    </distributionManagement>
    <dependencies>
        <!-- ######################  CATS ######################### -->
        <!-- commons -->
        <dependency>
            <groupId>org.catools</groupId>
            <artifactId>common.testng</artifactId>
            <version>${catools.version}</version>
        </dependency>
        <dependency>
            <groupId>org.catools</groupId>
            <artifactId>tms.etl</artifactId>
            <version>${catools.version}</version>
        </dependency>
        <!-- ######################  /CATS ######################### -->
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>4.1.1</version>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>RELEASE</version>
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.0.1</version>
                <configuration>
                    <additionalOptions>
                        <additionalOption>-Xdoclint:none</additionalOption>
                    </additionalOptions>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>post-site</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                    <configuration>
                        <source>${maven.compiler.source}</source>
                        <target>${maven.compiler.source}</target>
                        <release>${maven.compiler.source}</release>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.6</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <profiles>
        <profile>
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>release</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.0.1</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <additionalOptions>
                                <additionalOption>-Xdoclint:none</additionalOption>
                            </additionalOptions>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
